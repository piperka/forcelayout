CFLAGS=-I. -O3 -ggdb -std=gnu99 -march=native -fno-math-errno -fno-trapping-math -fno-signed-zeros -ffinite-math-only -mtune=native
#CFLAGS=-I. -std=gnu99 -g -march=native
LDFLAGS=-ljansson -lm -lpthread
OBJS=world.o force.o adjust.o sparsify.o worker.o
CC=gcc

all: forcelayout

forcelayout: $(OBJS)
	$(CC) -o forcelayout $(OBJS) $(LDFLAGS)

world.o: world.c world.h force.h adjust.h sparsify.h
	$(CC) -c $(CFLAGS) world.c

force.o: force.c force.h world.h adjust.h worker.h
	$(CC) -c $(CFLAGS) force.c

adjust.o: adjust.c adjust.h world.h worker.h
	$(CC) -c $(CFLAGS) adjust.c

sparsify.o: sparsify.c world.h worker.h
	$(CC) -c $(CFLAGS) sparsify.c

worker.o: worker.c worker.h
	$(CC) -c $(CFLAGS) worker.c

clean:
	rm -f $(OBJS) forcelayout
